# Pipeline Trigger Child

See [parent repo](https://gitlab.com/ripp.io/experiments/trigger-test-parent) for more details.

This repo is a pipeline that looks to be triggered by the parent repo pipeline.
The goal is to recieve variables and artifacts from the parent repo, without a direct reference to the parent, so it can be used by many parent repos.
